# Spring Boot Vendor Project

## Overview

This project is meant to utilize spring boot in order to spin up a quick Account/Contact relationship model

## Instructions
1. Please install JDK, if not already installed.
    1. https://docs.oracle.com/javase/10/install/installation-jdk-and-jre-macos.htm#JSJIG-GUID-2FE451B0-9572-4E38-A1A5-568B77B146DE
2. If you have Maven installed then great go to the run step!  If not:
	a. please click on this link to download the ZIP:
	http://mirrors.ocf.berkeley.edu/apache/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.zip
	b. Extract and move extracted folder to your home folder.
	c. Run in Terminal window:   $ sudo ln -s  ~/apache-maven-3.5.4/bin/mvn /usr/local/bin/mvn
	d. You will need to enter your password
	e. Run $ mvn —version

If that works you have installed Maven!

## RUN STEPS

3. From this directory, run:
    1. $ mvn spring-boot:run


## TESTING API STEPS
1. I would recommend using POSTMAN to Test:

####GET - Fetch account by ID:
- Endpoint:  /accountses/{id}  (GET)

####CREATE - Create a new account with 0 to N contacts:
- Endpoint: /accountses  (POST)
###### Payload
- see sample payload in /src/test/resources/sample_payload.json

####UPDATE - Update a new account by ID:
- Endpoint:  /accountses/{id} (PUT)
###### Payload
- see sample payload in /src/test/resources/sample_payload.json

#### DELETE - Delete coffee shop by ID:
- Endpoint:  /accountses/{id}  (DELETE)


## CONNECTING TO H2 DATABASE
1. You can connect to the database by going here:  http://localhost:8080/h2-console/
