INSERT INTO accounts (company_name, address_line_1, address_line_2, city, state, postal_code, country)
VALUES ('Best Company', '123 Main St', 'Unit A', 'Austin', 'TX', '78702', 'USA');

INSERT INTO accounts (company_name, address_line_1, address_line_2, city, state, postal_code, country)
VALUES ('Worst Company', '222 Wall St', 'Suite 200', 'Austin', 'TX', '78758', 'USA');

INSERT INTO accounts (company_name, address_line_1, address_line_2, city, state, postal_code, country)
VALUES ('Slamazon', '999 FooBar St', 'Unit A', 'Austin', 'TX', '78721', 'USA');



INSERT INTO contacts (name, email, address_line_1, address_line_2, city, state, postal_code, country, accounts_id)
VALUES ('Jimmy John', 'jjohn@gmail.com', '123 Monster St', 'Unit B', 'Austin', 'TX', '78702', 'USA', 1);

INSERT INTO contacts (name, email, address_line_1, address_line_2, city, state, postal_code, country, accounts_id)
VALUES ('Betty Crocker', 'bcrocker@gmail.com', '2221 Walberg St', 'Suite 200', 'Austin', 'TX', '78758', 'USA', 1);

INSERT INTO contacts (name, email, address_line_1, address_line_2, city, state, postal_code, country, accounts_id)
VALUES ('Smarty Pants', 'shopsmart@gmail.com', '900 6th St', 'Unit A', 'Austin', 'TX', '78721', 'USA', 1);


COMMIT;